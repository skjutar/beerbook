package com.skjutar.beerbook;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

public class SearchFragment extends Fragment {

	private GridView mListView;
	private SearchView mSearchField;
	private Button btnSearch;
	private static String KEY_SUCCESS = "success";
	private static String KEY_USERS = "users";
	private HashMap<String, String> result;
	private ArrayAdapter<String> adapter;
	private View myView;
	private SwingBottomInAnimationAdapter swing;
	private SlidingPaneLayout slidingLayout;
	protected String clickedUser;
	private ImageLoader imageLoader;
	public HashMap<String, URL> picResult;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		imageLoader = ImageLoader.getInstance();
		myView = inflater.inflate(R.layout.search, container, false);
		slidingLayout = (SlidingPaneLayout) myView.findViewById(R.id.searchSlider);
		slidingLayout.openPane();
		slidingLayout.setSliderFadeColor(getResources().getColor(R.color.greenlight));

		slidingLayout.setPanelSlideListener(new SimplePanelSlideListener());
		mListView = (GridView)myView.findViewById(R.id.searchList); 
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				clickedUser = (String) mListView.getItemAtPosition(position); 
				slidingLayout.closePane();

			}
		});
		result = new HashMap<String, String>();
		adapter = new TextAndPicAdapter(getActivity(), R.layout.search, result);
		swing = new SwingBottomInAnimationAdapter(adapter);
		swing.setAbsListView(mListView);
		mListView.setAdapter(swing);
		mSearchField = (SearchView)myView.findViewById(R.id.searchField);
		mSearchField.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String text) {
				((LoadSpinnerListener)getActivity()).setLoadingSpinnter(true);
				mSearchField.clearFocus();
				new SearchTask().execute(text);  
				return true;
			}

			@Override
			public boolean onQueryTextChange(String arg0) {
				return false;
			}
		});



		if(savedInstanceState!=null)
		{
			result = (HashMap<String, String>) savedInstanceState.getSerializable("items");
			
			adapter.addAll(new ArrayList<String>(result.keySet()));
			adapter.notifyDataSetChanged();
		}
		else {
			Fragment fragment = new UserDetailsFragment();
			FragmentManager     fm = getFragmentManager();		
			FragmentTransaction ft = fm.beginTransaction();
			ft.replace(R.id.user_details_right, fragment);
			ft.commit(); 
		}
		return myView;
	}

	@Override
	public void onAttach(Activity activity) {                

		if (! (activity instanceof SearchClickListener) )
			throw new ClassCastException();

		super.onAttach(activity);

	}



	public void closeDrawer()
	{
		slidingLayout.closePane();
	}
	
	public void openDrawer()
	{
		slidingLayout.openPane();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("items", (Serializable)result);
	}



	public boolean slidingState()
	{
		return slidingLayout.isOpen();
	}

	private class SearchTask extends AsyncTask<String, Void, JSONObject> {

		private String currentUser;

		protected JSONObject doInBackground(String... params) {
			BackEndService userFunction = new BackEndService();
			JSONObject json = userFunction.search(params[0]);
			currentUser = new DatabaseHandler(getActivity()).getUserDetails().get("name");
			return json;
		}

		protected void onPostExecute(JSONObject json) {
			try {
				if (json!= null && json.getString(KEY_SUCCESS)!= null) 
				{
					String res = json.getString(KEY_SUCCESS);
					if(Integer.parseInt(res) == 1){
						JSONArray jArray = json.getJSONArray(KEY_USERS);
						result = new HashMap<String,String>();
						for(int i=0; i<jArray.length(); i++)
						{
							String name = jArray.getJSONObject(i).getJSONObject("user").getString("NAME");
							String picURL = jArray.getJSONObject(i).getJSONObject("user").getString("picURL");
							if(!name.equals(currentUser))
									result.put(name, "http://graph.facebook.com/"+picURL+"/picture?type=large");
						}                    


						mListView.setVisibility(View.VISIBLE);

						((LoadSpinnerListener)getActivity()).setLoadingSpinnter(false);
						adapter.clear();
						adapter.addAll(new ArrayList<String>(result.keySet()));
						swing.notifyDataSetChanged(true);



					}else{
						((ProgressBar) myView.findViewById(R.id.loginProgress)).setVisibility(View.GONE);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}


	private class TextAndPicAdapter extends ArrayAdapter<String> {



		public TextAndPicAdapter(Context context, int textViewResourceId, HashMap<String, String> objects)
		{		
			super(context, R.layout.searchitem, R.id.itemText, new ArrayList<String>(objects.keySet()));
		}




		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = super.getView(position, convertView, parent);
			ImageView picView = (ImageView)row.findViewById(R.id.profilePicture);
			TextView textView = (TextView)row.findViewById(R.id.itemText);
			String text = super.getItem(position);
			if(result.get(text)!=null) {
					imageLoader.displayImage(result.get(text), picView);
					textView.setText(text);
			}
			return row;		
		}

	}



	private class SimplePanelSlideListener implements SlidingPaneLayout.PanelSlideListener
	{

		@Override
		public void onPanelClosed(View arg0) {
			((SearchClickListener) getActivity()).onSearchItemClick(clickedUser);		
		}

		@Override
		public void onPanelOpened(View arg0) {
			((FollowListener) getActivity()).followState(false);

		}

		@Override
		public void onPanelSlide(View arg0, float arg1) {
			// TODO Auto-generated method stub

		}

		//	

	}


}

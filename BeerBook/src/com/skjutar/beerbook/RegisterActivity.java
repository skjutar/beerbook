package com.skjutar.beerbook;

import org.json.JSONException;
import org.json.JSONObject;

 
 
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
 
public class RegisterActivity extends Activity {
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText inputPicURL;
    private TextView registerErrorMsg;
     
    // JSON Response node names
    private static String KEY_SUCCESS = "success";
    private static String KEY_ERROR = "error";
    private static String KEY_ERROR_MSG = "error_msg";
    private static String KEY_UID = "uid";
    private static String KEY_NAME = "name";
    private static String KEY_EMAIL = "email";
    private static String KEY_CREATED_AT = "created_at";
    private static String KEY_PIC = "pic";
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // Importing all assets like buttons, text fields
        inputFullName = (EditText) findViewById(R.id.registerName);
        inputEmail = (EditText) findViewById(R.id.registerEmail);
        inputPassword = (EditText) findViewById(R.id.registerPassword);
        inputPicURL= (EditText) findViewById(R.id.registerPic);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        registerErrorMsg = (TextView) findViewById(R.id.register_error);
         
        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {         
            public void onClick(View view) {
            	   String name = inputFullName.getText().toString();
            	    String email = inputEmail.getText().toString();
            	    String password = inputPassword.getText().toString();
            	    String picURL = inputPicURL.getText().toString();
            	    ((ProgressBar) findViewById(R.id.regProgress)).setVisibility(View.VISIBLE);
            	    new RegisterTask().execute(name, email, password, picURL);
            }
        });
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        // Respond to the action bar's Up/Home button
        case android.R.id.home:
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    
    
    private class RegisterTask extends AsyncTask<String, Void, JSONObject> {
        
        protected JSONObject doInBackground(String... params) {
                BackEndService userFunction = new BackEndService();
                if (params.length != 4)
                        return null;
                JSONObject json = userFunction.registerUser(params[0], params[1], params[2], params[3]);
                return json;
        }
       
        protected void onPostExecute(JSONObject json) {
                // check for login response
        try {
            if (json != null && json.getString(KEY_SUCCESS) != null) {
                registerErrorMsg.setText("");
                String res = json.getString(KEY_SUCCESS);
                if(Integer.parseInt(res) == 1){
                    // user successfully registred
                    // Store user details in SQLite Database
                    DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    JSONObject json_user = json.getJSONObject("user");
                     
                    // Clear all previous data in database
                    BackEndService userFunction = new BackEndService();
                    userFunction.logoutUser(getApplicationContext());
                    db.addUser(json_user.getString(KEY_NAME), json_user.getString(KEY_EMAIL), json.getString(KEY_UID), json_user.getString(KEY_CREATED_AT), json_user.getString(KEY_PIC));                        
                    // Launch Dashboard Screen
                    ((ProgressBar) findViewById(R.id.regProgress)).setVisibility(View.GONE);
                    Intent dashboard = new Intent(getApplicationContext(), MainActivity.class);
                    // Close all views before launching Dashboard
                    dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(dashboard);
                    // Close Registration Screen
                    finish();
                }else{
                    // Error in registration
                    registerErrorMsg.setText("Error occured in registration");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        }
}
}

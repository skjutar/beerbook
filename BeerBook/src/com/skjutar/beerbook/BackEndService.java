package com.skjutar.beerbook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class BackEndService {



	private JSONParser jsonParser;


	


	private static String URL = "http://46.239.117.153/android_login_api/";

	private static String login_tag = "login";
	private static String register_tag = "register";
	private static String search = "search";
	private static final String getBeverage = "getBeverage";
	private static final String addBeverageToUser = "addBeverageToUser";
	private static final String searchBeverages = "searchBeverages";
	private static final String addBeverageToUserByArtikel = "addBeverageToUserByArtikel";
	private static final String getUserBeverages = "getUserBeverages";
	private static final String removeUserBeverage = "removeUserBeverage";
	private static final String getPic = "getPic";
	private static final String addFriend = "addFriend";
	private static final String removeFriend = "removeFriend";
	private static final String checkFriend = "checkFriend";
	private static final String getFriends = "getFriends";
	private static final String getFeed = "getFeed";

	// constructor
	public BackEndService(){
		jsonParser = new JSONParser();
	}

	/**
	 * function make Login Request
	 * @param email
	 * @param password
	 * */
	public JSONObject loginUser(String email, String password){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", login_tag));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		System.out.println("Email: " + email + " password: "+password);
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}

	/**
	 * function make Login Request
	 * @param name
	 * @param email
	 * @param password
	 * @param picURL 
	 * */
	public JSONObject registerUser(String name, String email, String password, String picURL){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", register_tag));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("pic", picURL));
		// getting JSON Object
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		// return json
		return json;
	}

	public JSONObject search(String name)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", search));
		params.add(new BasicNameValuePair("name", name));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		
		return json;
	}

	/**
	 * Function get Login status
	 * */
	public boolean isUserLoggedIn(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		int count = db.getRowCount();
		if(count > 0){
			// user logged in
			return true;
		}
		return false;
	}

	/**
	 * Function to logout user
	 * Reset Database
	 * */
	public boolean logoutUser(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		db.resetTables();
		return true;
	}

	public JSONObject getBeverage(String id) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", getBeverage));
		params.add(new BasicNameValuePair("id", id));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}

	public JSONObject addBeverageToList(String beverage,String user) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", addBeverageToUser));
		params.add(new BasicNameValuePair("beverage", beverage));
		params.add(new BasicNameValuePair("user", user));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		Log.d("addBeverageToList", "from backend: " + json.toString());
		return json;
	}

	public JSONObject searchBeverages(String beverage) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", searchBeverages));
		params.add(new BasicNameValuePair("beverage", beverage));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
		
	}

	public JSONObject addBeverageToListByArtikel(String artikel, String name, String ean) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", addBeverageToUserByArtikel));
		params.add(new BasicNameValuePair("beverage", artikel));
		params.add(new BasicNameValuePair("user", name));
		params.add(new BasicNameValuePair("ean", ean));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}

	public JSONObject getUserBeverages(String name) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", getUserBeverages));
		params.add(new BasicNameValuePair("name", name));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}
	
	public JSONObject removeUserBeverage(String name, String beverage){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", removeUserBeverage));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("beverage", beverage));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;		
	}

	public JSONObject getUserPic(String name) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", getPic));
		params.add(new BasicNameValuePair("name", name));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}

	public JSONObject setFriend(String user, String friend) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", addFriend));
		params.add(new BasicNameValuePair("name", user));
		params.add(new BasicNameValuePair("friend", friend));
		Log.d("json", "name: " + user + " friend: "+friend);
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}

	public JSONObject setUnFriend(String user, String friend) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", removeFriend));
		params.add(new BasicNameValuePair("name", user));
		params.add(new BasicNameValuePair("friend", friend));
		Log.d("json", "name: " + user + " friend: "+friend);
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}

	public JSONObject checkFriend(String user, String friend) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", checkFriend));
		params.add(new BasicNameValuePair("name", user));
		params.add(new BasicNameValuePair("friend", friend));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}

	public JSONObject getFriends(String currentUser) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", getFriends));
		params.add(new BasicNameValuePair("name", currentUser));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json;
	}

	public JSONObject getFeed(String userName, int page) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", getFeed));
		params.add(new BasicNameValuePair("name", userName));
		params.add(new BasicNameValuePair("page", page+""));
		JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params);
		return json; 
	}
	
	

}

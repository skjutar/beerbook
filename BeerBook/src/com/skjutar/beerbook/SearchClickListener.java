package com.skjutar.beerbook;

import android.widget.AdapterView.OnItemClickListener;

public interface SearchClickListener {

	public void onSearchItemClick(String user);
	public void closeDrawer();
}

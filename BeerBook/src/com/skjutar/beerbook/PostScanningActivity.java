package com.skjutar.beerbook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;



public class PostScanningActivity extends Activity {

	public static final String KEY_RESULT = "result";
	private static String KEY_SUCCESS = "success";
	private static String KEY_ERROR = "error";
	private static String KEY_ERROR_MSG = "error_msg";
	private String ean;
	public ArrayList<String> result;
	private ListView mListView;
	private ArrayAdapter<String> adapter;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading);
		result = new ArrayList<String>();
		adapter = new DoubleArrayAdapter(this, result); //<String>(this, R.layout.searchitem, result);
		Intent intent = getIntent();
		ean = intent.getExtras().getString("result");
		new GetBeverageTask().execute(ean);    
	}




	private class GetBeverageTask extends AsyncTask<String, Void, JSONObject> {

		private String name = "";
		@Override
		protected JSONObject doInBackground(String... params) {
			BackEndService backend = new BackEndService();
			JSONObject json = backend.getBeverage(params[0]);
			backend=null;
			return json;
		}

		protected void onPostExecute(JSONObject json) {
			setContentView(R.layout.postscanning);
			mListView = (ListView)findViewById(R.id.searchBeveragesList);
			mListView.setAdapter(adapter);
			try {
				if(json.getString(KEY_SUCCESS).equals("1"))
				{
					EditText editText = (EditText) findViewById(R.id.scanName);
					JSONObject beverage = json.getJSONObject("beverage");
					name = beverage.getString("Namn");
					editText.setText(name+", "+ 
							beverage.getString("Alkoholhalt")+" \n"+
							beverage.getString("Ursprunglandnamn")+"\n"+
							beverage.getString("Volym")+"\n"+
							beverage.getString("Forpackning"));

					Button button = (Button) findViewById(R.id.buttonPostScan);
					button.setOnClickListener(new OnClickListener() {						
						@Override
						public void onClick(View v) {
							setContentView(R.layout.loading);
							new AddBeverageToListTask().execute(name, "ean");  
						}
					});

				}
				else {
					TextView errorText = (TextView) findViewById(R.id.add_err);
					errorText.setText("That beverage is not in our database, please add it!");
					errorText.setVisibility(View.VISIBLE);
					TextView scanName = (TextView) findViewById(R.id.scanName);
					scanName.setInputType(InputType.TYPE_CLASS_TEXT);
					Button button = (Button) findViewById(R.id.buttonPostScan);
					button.setText("Search");
					button.setOnClickListener(new OnClickListener() {						
						@Override
						public void onClick(View v) {
							TextView input = (TextView) findViewById(R.id.scanName);
							String stringInput = input.getText().toString();
							new SearchTask().execute(stringInput);
							((ProgressBar) findViewById(R.id.searchBeverageProgress)).setVisibility(View.VISIBLE);
						}
					});
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

	}

	private class SearchTask extends AsyncTask<String, Void, JSONObject> {


		@Override
		protected JSONObject doInBackground(String... params) {
			BackEndService backend = new BackEndService();
			JSONObject json = backend.searchBeverages(params[0]);
			return json;
		}
		protected void onPostExecute(JSONObject json) {
			try {
				if (json != null && json.getString(KEY_SUCCESS) != null) 
				{
					String res = json.getString(KEY_SUCCESS);
					if(Integer.parseInt(res) == 1){
						JSONArray jArray = json.getJSONArray(KEY_RESULT);
						result= new ArrayList<String>();
						for(int i=0; i<jArray.length(); i++)
						{
							//Log.d("searchResult", "name: "+ jArray.getJSONObject(i).getJSONObject("user").getString("NAME"));
							JSONObject j = jArray.getJSONObject(i);
							//result.add(j.toString());
							j = j.getJSONObject("beverage");
							result.add(j.getString("Namn") + "-"+ j.getString("Alkoholhalt")+" "+j.getString("Volymiml")+"ml "+ j.getString("Forpackning")+ " nr:"+ j.getString("Artikelid"));
						}                    

						((ProgressBar) findViewById(R.id.searchBeverageProgress)).setVisibility(View.GONE);
						mListView.setVisibility(View.VISIBLE);
						mListView.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> arg0,
									View view, int position, long id) {
								String clickedItem = (String) mListView.getItemAtPosition(position);
								String[] splitted = clickedItem.split(":");
								setContentView(R.layout.loading);
								new AddBeverageToListTask().execute(splitted[1], "artikel", ean);
							}
						});
						adapter.clear();
						adapter.addAll(result);
						adapter.notifyDataSetChanged();
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private class AddBeverageToListTask extends AsyncTask<String, Void, JSONObject> {

		@Override
		protected JSONObject doInBackground(String... params) {
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			HashMap<String, String> user = db.getUserDetails();
			String name = user.get("name");
			BackEndService backend = new BackEndService();
			JSONObject json;
			if(params[1].equals("ean"))
			{
				 json = backend.addBeverageToList(ean, name);
			}
			else {
				 json = backend.addBeverageToListByArtikel(params[0], name, ean);
			}
			return json;
		}

		protected void onPostExecute(JSONObject json) {
			Toast.makeText(getApplicationContext(), json.toString(), Toast.LENGTH_LONG).show();
			finish();
		}

	}

	private class DoubleArrayAdapter extends ArrayAdapter<String>
	{

		private Context context;
		private List<String> values;

		public DoubleArrayAdapter(Context context, List<String> objects) {
			super(context,R.layout.doublesearchitem, objects);
			this.context = context;
			this.values = objects;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.doublesearchitem, parent, false);
			TextView textBig = (TextView) rowView.findViewById(R.id.doubleBig);
			TextView textSmall = (TextView) rowView.findViewById(R.id.doubleSmall);
			String text = values.get(position);
			String[] splitted = text.split("-");
			textBig.setText(splitted[0]);
			textSmall.setText(splitted[1]);
			return rowView;
		}

	}

	@Override
	public void onBackPressed()
	{
		finish();
		super.onBackPressed();
	}

}

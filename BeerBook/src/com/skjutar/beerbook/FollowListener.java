package com.skjutar.beerbook;

public interface FollowListener {
	public void followState(boolean state);
	public void setFollow(boolean state);
}

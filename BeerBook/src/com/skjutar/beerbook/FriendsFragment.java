package com.skjutar.beerbook;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;



import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SearchView.OnQueryTextListener;

public class FriendsFragment extends Fragment {
	
	public static final String KEY_RESULT = "result";
	private GridView mListView;
	private static String KEY_SUCCESS = "success";
	private static String KEY_USERS = "users";
	private HashMap<String, URL> result;
	private ArrayAdapter<String> adapter;
	private View myView;
	private SwingBottomInAnimationAdapter swing;
	protected String clickedUser;
	private ImageLoader imageLoader;
	private LoadSpinnerListener loadSpinnerListener;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		imageLoader = ImageLoader.getInstance();
		myView = inflater.inflate(R.layout.friends, container, false);
		mListView = (GridView)myView.findViewById(R.id.friendList); 
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				String user = (String) mListView.getItemAtPosition(position); 
				Intent i = new Intent(getActivity(), UserDetails.class);
				i.putExtra("user", user);
				startActivity(i);  
			}
		});

		result = new HashMap<String, URL>();
		adapter = new TextAndPicAdapter(getActivity(), R.layout.search, result);
		swing = new SwingBottomInAnimationAdapter(adapter);
		swing.setAbsListView(mListView);
		mListView.setAdapter(swing);
		


		if(savedInstanceState!=null)
		{
			result = (HashMap<String, URL>) savedInstanceState.getSerializable("items");
			
			adapter.addAll(new ArrayList<String>(result.keySet()));
			adapter.notifyDataSetChanged();
		}
		else
		{
			loadSpinnerListener.setLoadingSpinnter(true);
			new GetFriendTask().execute();
		}
		return myView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			loadSpinnerListener = (LoadSpinnerListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}



	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("items", (Serializable)result);
		
	}





	private class GetFriendTask extends AsyncTask<Void, Void, JSONObject> {

		private String currentUser;

		protected JSONObject doInBackground(Void... params) {
			currentUser = new DatabaseHandler(getActivity()).getUserDetails().get("name");
			BackEndService userFunction = new BackEndService();
			JSONObject json = userFunction.getFriends(currentUser);		
			return json;
		}

		protected void onPostExecute(JSONObject json) {
			try {
				if (json!= null && json.getString(KEY_SUCCESS)!= null) 
				{
					String res = json.getString(KEY_SUCCESS);
					if(Integer.parseInt(res) == 1){
						JSONArray jArray = json.getJSONArray(KEY_RESULT);
						result= new HashMap<String, URL>();
						for(int i=0; i<jArray.length(); i++)
						{
							String name = jArray.getJSONObject(i).getJSONObject("friend").getString("name");
							String picURL = jArray.getJSONObject(i).getJSONObject("friend").getString("picURL");
							if(!name.equals(currentUser))
								try {
									result.put(name, new URL("http://graph.facebook.com/"+picURL+"/picture?type=normal"));
								} catch (MalformedURLException e) {
									e.printStackTrace();
								}
						}                    

						if(adapter==null)
						{
							adapter = new TextAndPicAdapter(getActivity(), R.layout.search, result);
							mListView.setAdapter(adapter);
						}
						mListView.setVisibility(View.VISIBLE);

						loadSpinnerListener.setLoadingSpinnter(false);
						adapter.clear();
						adapter.addAll(new ArrayList<String>(result.keySet()));
						swing.notifyDataSetChanged(true);



					}else{
						((ProgressBar) myView.findViewById(R.id.loginProgress)).setVisibility(View.GONE);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}
	
	
	
	
	private class TextAndPicAdapter extends ArrayAdapter<String> {



		public TextAndPicAdapter(Context context, int textViewResourceId, HashMap<String, URL> objects)
		{		
			super(context, R.layout.searchitem, R.id.itemText, new ArrayList<String>(objects.keySet()));
		}




		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = super.getView(position, convertView, parent);
			ImageView picView = (ImageView)row.findViewById(R.id.profilePicture);
			TextView textView = (TextView)row.findViewById(R.id.itemText);
			String text = super.getItem(position);
			if(result.get(text)!=null) {
				try {
					imageLoader.displayImage(result.get(text).toURI().toString(), picView);
					textView.setText(text);
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
			return row;		
		}

	}





	
	
	



}




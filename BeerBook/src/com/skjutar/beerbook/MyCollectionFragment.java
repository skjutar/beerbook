package com.skjutar.beerbook;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.haarman.listviewanimations.itemmanipulation.AnimateDismissAdapter;
import com.haarman.listviewanimations.itemmanipulation.ExpandableListItemAdapter;
import com.haarman.listviewanimations.itemmanipulation.OnDismissCallback;
import com.haarman.listviewanimations.itemmanipulation.SwipeDismissAdapter;
import com.haarman.listviewanimations.swinginadapters.prepared.AlphaInAnimationAdapter;
import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.haarman.listviewanimations.swinginadapters.prepared.SwingLeftInAnimationAdapter;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;



public class MyCollectionFragment extends Fragment {

	private MyExpandableListItemAdapter mExpandableListItemAdapter;
	private boolean mLimited;
	private ListView mListView;
	public ArrayList<HashMap<String, String>> result;
	private View mView;
	private SwingLeftInAnimationAdapter alphaInAnimationAdapter;
	private SwipeDismissAdapter adapter;
	private static AnimateDismissAdapter<String> animateDismissAdapter;
	private static final String KEY_SUCCESS = "success";
	public static final String KEY_BEVERAGES = "beverages";
	private LoadSpinnerListener loadSpinnerListener;
	private View header;
	public Bitmap profilePicture;
	public String userName;



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.my_collection, container, false);
		mListView = (ListView)mView.findViewById(R.id.mycollectionlist);
		mListView.setDivider(null); 
		header = inflater.inflate(R.layout.my_collection_header, null, false);
		mListView.addHeaderView(header);
		mExpandableListItemAdapter = new MyExpandableListItemAdapter(getActivity(), result);
		mExpandableListItemAdapter.setLimit(1);
		animateDismissAdapter = new AnimateDismissAdapter<String>(mExpandableListItemAdapter, new OnDismissCallback() {

			@Override
			public void onDismiss(AbsListView listView, int[] reverseSortedPositions) {
				for (int position : reverseSortedPositions) {
					mExpandableListItemAdapter.remove(position);
				}

			}
		});
		SwingBottomInAnimationAdapter swing = new SwingBottomInAnimationAdapter(animateDismissAdapter);
		swing.setAbsListView(mListView);

		mListView.setAdapter(swing);
		if(savedInstanceState==null)
		{
			loadSpinnerListener.setLoadingSpinnter(true);
			new FillMyCollectionTask().execute();
		}
		else{
			//result = savedInstanceState.getStringArrayList("list");
			result = (ArrayList<HashMap<String,String>>)savedInstanceState.getSerializable("items");
			profilePicture = savedInstanceState.getParcelable("pic");
			mExpandableListItemAdapter.addAll(result);
			mExpandableListItemAdapter.notifyDataSetChanged();
			((ImageView)header.findViewById(R.id.profilePicture)).setImageBitmap(profilePicture);
		}

		return mView;
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("items", (Serializable) result);
		outState.putParcelable("pic", profilePicture);
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			loadSpinnerListener = (LoadSpinnerListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}



	private class FillMyCollectionTask extends AsyncTask<Void, Void, JSONObject> {



		@Override
		protected JSONObject doInBackground(Void...voids) {
			DatabaseHandler db = new DatabaseHandler(getActivity());
			HashMap<String, String> user = db.getUserDetails();
			userName = user.get("name");
			String picURL = user.get("pic");
			URL img_value;
			try {
				img_value = new URL("http://graph.facebook.com/"+picURL+"/picture?type=large");
				profilePicture = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BackEndService backend = new BackEndService();
			JSONObject json = backend.getUserBeverages(userName);
			return json;
		}

		protected void onPostExecute(JSONObject json) {
			try {
				if (json != null && json.getString(KEY_SUCCESS) != null) 
				{
					String res = json.getString(KEY_SUCCESS);
					if(Integer.parseInt(res) == 1){
						JSONArray jArray = json.getJSONArray(KEY_BEVERAGES);
						result= new ArrayList<HashMap<String,String>>();
						for(int i=0; i<jArray.length(); i++)
						{
							JSONObject j = jArray.getJSONObject(i);
							j = j.getJSONObject("beverage");
							HashMap<String, String> item = new HashMap<String, String>();
							item.put("title", j.getString("Namn") + " "+ j.getString("Alkoholhalt"));
							item.put("id", j.getString("Artikelid"));
							item.put("content", "Date: "+
									j.getString("DATE") + "\n"+"Volume: "+
									j.getString("Volymiml")+"ml"+ "\n"+"Package: "+
									j.getString("Forpackning"));
							item.put("rating", j.getString("RATING"));
							result.add(item);
						}
						ImageView profilePic = (ImageView)header.findViewById(R.id.profilePicture);
						TextView userNameView = (TextView)header.findViewById(R.id.headerText);
						userNameView.setText(userName);
						profilePic.setImageBitmap(profilePicture);

						mExpandableListItemAdapter.clear();
						mExpandableListItemAdapter.addAll(result);
						mExpandableListItemAdapter.notifyDataSetChanged();
						loadSpinnerListener.setLoadingSpinnter(false);
					}
				}
			}catch (JSONException e){

			}

		}

	}


	private class MyExpandableListItemAdapter extends ExpandableListItemAdapter<HashMap<String, String>> {

		private Context mContext;

		/**
		 * Creates a new ExpandableListItemAdapter with the specified list, or an empty list if
		 * items == null.
		 */
		private MyExpandableListItemAdapter(Context context, List<HashMap<String, String>> items) {
			super(context, R.layout.my_collection_card, R.id.activity_expandablelistitem_card_title, R.id.activity_expandablelistitem_card_content, items);
			mContext = context;
		}



		@Override
		public View getTitleView(int position, View convertView, ViewGroup parent) {
			TextView tv = (TextView) convertView;
			if (tv == null) {
				tv = new TextView(mContext);
			}
			HashMap<String, String> item = getItem(position);
			tv.setTextSize(25);
			tv.setText(item.get("title"));
			//tv.setTextColor(Color.parseColor("#222324"));
			tv.setTextColor(Color.parseColor("#787B82"));
			tv.setTypeface(Typeface.DEFAULT);
			return tv;
		}

		@Override
		public View getContentView(int position, View convertView, ViewGroup parent) {
			View view = parent.findViewById(R.id.activity_expandablelistitem_card_content);
			View layout = view.findViewById(R.id.activity_expandablelistitem_card_content_layout);
			TextView textView = (TextView) layout.findViewById(R.id.textExpandedBeverage);
			TextView hidden = (TextView) layout.findViewById(R.id.hiddenText);
			TextView beverageName = (TextView) layout.findViewById(R.id.beverageName);
			TextView beverageID = (TextView) layout.findViewById(R.id.beverageID);
			hidden.setText(""+position);
			Button deleteButton =(Button) layout.findViewById(R.id.btnDeleteBeverage);
			deleteButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					View parent = (View) v.getParent();
					String position = ((TextView) parent.findViewById(R.id.hiddenText)).getText().toString();
					String beverageID = ((TextView) parent.findViewById(R.id.beverageID)).getText().toString();
					new RemoveBeverageTask().execute(beverageID);
					animateDismissAdapter.animateDismiss(Integer.parseInt(position));
					result.remove(Integer.parseInt(position));
					v.setId(R.id.btnDeleteBeverage);
				}
			});

			Button infoButton = (Button) layout.findViewById(R.id.btnInfoBeverage);
			infoButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {					
					Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
					View parent = (View) v.getParent();
					String name = ((TextView) parent.findViewById(R.id.beverageName)).getText().toString();
					intent.putExtra(SearchManager.QUERY, name);
					if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
						getActivity().startActivity(intent);
					} else {
						Toast.makeText(mContext, "App not avaiable", Toast.LENGTH_LONG).show();
					}

				}
			});

			RatingBar rBar = (RatingBar) layout.findViewById(R.id.ratingBar);
			rBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

				@Override
				public void onRatingChanged(RatingBar ratingBar, float rating,
						boolean fromUser) {
					if(fromUser)
						Toast.makeText(mContext, "Value: "+rating, Toast.LENGTH_LONG).show();

				}
			});
			HashMap<String, String> item = getItem(position);
			rBar.setRating(Float.parseFloat(item.get("rating")));
			textView.setText(item.get("content"));
			textView.setTypeface(Typeface.DEFAULT);
			beverageID.setText(item.get("id"));
			beverageName.setText(item.get("title"));

			return layout;
		}

		private class RemoveBeverageTask extends AsyncTask<String, Void, JSONObject> {

			@Override
			protected JSONObject doInBackground(String... params) {
				DatabaseHandler db = new DatabaseHandler(mContext);
				HashMap<String, String> user = db.getUserDetails();
				String name = user.get("name");
				BackEndService backend = new BackEndService();
				JSONObject json = backend.removeUserBeverage(name, params[0]);

				return json;
			}

			protected void onPostExecute(JSONObject json) {
				Toast.makeText(mContext, json.toString(), Toast.LENGTH_LONG).show();
			}



		}





	}






}


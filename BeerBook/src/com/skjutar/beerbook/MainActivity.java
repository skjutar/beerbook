/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skjutar.beerbook;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends Activity implements LoadSpinnerListener, SearchClickListener, FollowListener {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mDrawerTitles;
	private Fragment fragment;
	private NewsFragment newsFragment;
	private MyCollectionFragment myCollectionFragment;
	private boolean changeFragment=true;
	private BackEndService userFunctions;
	private SearchFragment searchFragment;
	private LoadingFragment loadingFragment;
	private Menu menu;
	private int currentView=0;
	private FriendsFragment friendsFragment;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userFunctions = new BackEndService();
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration
				.Builder(getApplicationContext())
		.defaultDisplayImageOptions(defaultOptions)
		.build();
		ImageLoader.getInstance().init(config);

		if(userFunctions.isUserLoggedIn(getApplicationContext())){
			setContentView(R.layout.activity_main);

			Log.d("drawer", "oncreateCalled");

			loadingFragment = new LoadingFragment();

			mTitle = mDrawerTitle = getTitle();
			mDrawerTitles = getResources().getStringArray(R.array.drawer_array);
			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawerList = (ListView) findViewById(R.id.left_drawer);
			// set a custom shadow that overlays the main content when the drawer opens
			mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);


			// set up the drawer's list view with items and click listener
			mDrawerList.setAdapter(new ArrayAdapter<String>(this,
					R.layout.drawer_list_item, mDrawerTitles));
			mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

			// enable ActionBar app icon to behave as action to toggle nav drawer
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setHomeButtonEnabled(true);

			mDrawerToggle = new ActionBarDrawerToggle(
					this,                  /* host Activity */
					mDrawerLayout,         /* DrawerLayout object */
					R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
					R.string.drawer_open,  /* "open drawer" description for accessibility */
					R.string.drawer_close  /* "close drawer" description for accessibility */
					) {
				public void onDrawerClosed(View view) {
					Log.d("drawer", "onDrawerClosed kallad");
					getActionBar().setTitle(mTitle);
					invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
					if(changeFragment && fragment!=null)
					{
						Log.d("drawer", "changing fragment");
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
						changeFragment=false;
					}

				}

				public void onDrawerOpened(View drawerView) {
					Log.d("drawer", "onDrawerOpened kallad");
					getActionBar().setTitle(mDrawerTitle);
					invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
				}
			};
			mDrawerLayout.setDrawerListener(mDrawerToggle);

			if (savedInstanceState == null) {
				selectItem(0);
			}

			if(fragment!=null)
			{
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			}
		}
		else {
			// user is not logged in show login screen
			Intent login = new Intent(getApplicationContext(), LoginActivity.class);
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			// Closing dashboard screen
			finish();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("currentView", currentView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}



	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Log.d("drawer", "onPrepareOptionsMenu kallad");
		// If the nav drawer is open, hide action items related to the content view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.scan).setVisible(!drawerOpen);
		menu.findItem(R.id.logout).setVisible(drawerOpen);
		//menu.findItem(R.id.follow).setVisible(true);
		//menu.findItem(R.id.menusearchBar).setVisible(showSearch);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			changeFragment=false;
			return true;
		}
		// Handle action buttons
		switch(item.getItemId()) {
		case R.id.follow:
			UserDetailsFragment fragment = (UserDetailsFragment) getFragmentManager()	
			.findFragmentById(R.id.user_details_right);
			if(item.getTitle().equals(getString(R.string.follow)))
			{
				fragment.setFriend(true);
			}
			else 
			{
				fragment.setFriend(false);
			}
			return true;

		case R.id.scan:
			performScanning() ;
			return true;
		case R.id.logout:
			userFunctions.logoutUser(getApplicationContext());
			Intent login = new Intent(getApplicationContext(), LoginActivity.class);
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			// Closing Main screen
			finish();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}

	}

	private void selectItem(int position) {
		Log.d("drawer", "selectItem kallad");
		currentView=position;
		switch(position)
		{
		case 0:

			newsFragment= new NewsFragment();

			fragment =  newsFragment;
			break;

		case 1:

			myCollectionFragment= new MyCollectionFragment();

			fragment =  myCollectionFragment;
			break;

		case 2:
			friendsFragment = new FriendsFragment();

			fragment = friendsFragment;
			break;
		case 3:
			//if(searchFragment==null)
			searchFragment = new SearchFragment();
			fragment = searchFragment;
			break;


		}
		mDrawerList.setItemChecked(position, true);

		setProgressBarIndeterminateVisibility(true); 

		setTitle(mDrawerTitles[position]);
		changeFragment=true; 
		mDrawerLayout.closeDrawer(mDrawerList);


	}

	public void performScanning() {
		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE", "QR_CODE_MODE");
		startActivityForResult(intent, 0);
	}


	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				intent.getStringExtra("SCAN_RESULT_FORMAT");
				Intent i = new Intent(this, PostScanningActivity.class);
				i.putExtra("result", contents);
				startActivity(i);
				// Handle successful scan
				Toast.makeText(getApplicationContext(), contents, Toast.LENGTH_LONG).show();
			} else if (resultCode == RESULT_CANCELED) {

			}
		}
	}



	@Override
	public void setTitle(CharSequence title) {	
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}





	@Override
	public void setLoadingSpinnter(boolean spinning) {
		MenuItem refresh = menu.findItem(R.id.menuRefresh);
		if(spinning){
			refresh.setActionView(R.layout.actionbar_indeterminate_progress);
		}
		else {
			refresh.setActionView(null);
		}


	}

	@Override
	public void onBackPressed()
	{
		if(searchFragment!=null && searchFragment.isAdded() && !searchFragment.slidingState())
		{
			searchFragment.openDrawer();
		}
		else super.onBackPressed();
	}

	@Override
	public void onSearchItemClick(String user){
		UserDetailsFragment fragment = (UserDetailsFragment) getFragmentManager().findFragmentById(R.id.user_details_right);
		fragment.changeUser(user);
	}

	@Override
	public void closeDrawer() {
		searchFragment.closeDrawer();
	}

	@Override
	public void followState(boolean state) {
		MenuItem follow = menu.findItem(R.id.follow);
		follow.setVisible(state);

	}

	@Override
	public void setFollow(boolean state) {
		MenuItem follow = menu.findItem(R.id.follow);
		if(state)
		{
			follow.setTitle(getString(R.string.unFollow));
		}
		else 
		{
			follow.setTitle(getString(R.string.follow));
		}

	}

}
/*
 * Copyright 2013 Niek Haarman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.skjutar.beerbook;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.CursorJoiner.Result;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.haarman.listviewanimations.ArrayAdapter;
import com.haarman.listviewanimations.itemmanipulation.OnDismissCallback;
import com.haarman.listviewanimations.itemmanipulation.SwipeDismissAdapter;
import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * TODO
 * @author kristofferskjutar
 *
 */
public class NewsFragment extends Fragment {

	private GoogleCardsAdapter mGoogleCardsAdapter;
	private static final String KEY_SUCCESS = "success";
	private static final String KEY_RESULT = "result";
	private SwingBottomInAnimationAdapter swingBottomInAnimationAdapter;
	private ListView rootView;
	private String userName;
	private int page;
	private ArrayList<HashMap<String, String>> result;
	private ImageLoader imageLoader;
	private Button moreButton;
	private LoadSpinnerListener loadSpinnerListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		imageLoader = ImageLoader.getInstance();
		mGoogleCardsAdapter = new GoogleCardsAdapter(getActivity());
		swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(mGoogleCardsAdapter);
		//'swingBottomInAnimationAdapter.setShouldAnimateFromPosition(3);
		result = new ArrayList<HashMap<String,String>>();
		mGoogleCardsAdapter.addAll(result);
		page=0;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(rootView==null)
		{
			rootView = (ListView) inflater.inflate(R.layout.activity_googlecards, container, false);
			moreButton =(Button) inflater.inflate(R.layout.feed_bottom, null, false);
			moreButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					loadSpinnerListener.setLoadingSpinnter(true);
					new GetFeedTask().execute();

				}
			});
			rootView.addFooterView(moreButton);
			swingBottomInAnimationAdapter.setAbsListView(rootView);
			rootView.setAdapter(swingBottomInAnimationAdapter);
			rootView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					TextView title =(TextView)view.findViewById(R.id.feed_card_title);
					Intent i = new Intent(getActivity(), UserDetails.class);
					i.putExtra("user", title.getText().toString());
					startActivity(i);  

				}
			});
		}
		if(savedInstanceState!=null)
		{
			result = (ArrayList<HashMap<String, String>>) savedInstanceState.getSerializable("items");
			mGoogleCardsAdapter.addAll(result);
		}
		else {
			new GetUsernameTask().execute();
		}
		return rootView;	
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			loadSpinnerListener = (LoadSpinnerListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("items", (Serializable) result);
	}


	private class GetUsernameTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			DatabaseHandler db = new DatabaseHandler(getActivity());
			HashMap<String, String> user = db.getUserDetails();
			userName = user.get("name");
			return userName;
		}

		protected void onPostExecute(String userName) {
			new GetFeedTask().execute();
		}
	}

	private class GetFeedTask extends AsyncTask<Void, Void, ArrayList<HashMap<String, String>>> {

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(Void... arg0) {
			BackEndService backend = new BackEndService();
			JSONObject json = backend.getFeed(userName, page++);
			ArrayList<HashMap<String, String>> newItems = new ArrayList<HashMap<String,String>>();
			try {
				if (json != null && json.getString(KEY_SUCCESS) != null) 
				{
					String res = json.getString(KEY_SUCCESS);
					if(Integer.parseInt(res) == 1){
						JSONArray jArray = json.getJSONArray(KEY_RESULT);		
						for(int i=0; i<jArray.length(); i++)
						{
							JSONObject j = jArray.getJSONObject(i);
							j = j.getJSONObject("instance");
							HashMap<String, String> item = new HashMap<String, String>();
							item.put("PicURL", "http://graph.facebook.com/"+j.getString("PicURL")+"/picture?type=normal");
							item.put("title", j.getString("Namn") + " "+ j.getString("Alkoholhalt"));
							item.put("id", j.getString("Artikelid"));
							item.put("content", "Date: "+
									j.getString("DATE") + "\n"+"Volume: "+
									j.getString("Volymiml")+"ml"+ "\n"+"Package: "+
									j.getString("Forpackning"));
							item.put("rating", j.getString("RATING"));
							item.put("user", j.getString("FRIEND"));
							newItems.add(item);
						}
					}
				}
			}catch (JSONException e){
				e.printStackTrace();
			}
			return newItems;
		}

		protected void onPostExecute(ArrayList<HashMap<String, String>> newItems) {
			if(newItems!=null) 
			{	
				result.addAll(newItems);
				mGoogleCardsAdapter.addAll(newItems);
				mGoogleCardsAdapter.notifyDataSetChanged();
				if(newItems.size()<20)
				{
					moreButton.setVisibility(View.GONE);
				}
				else {
					moreButton.setVisibility(View.VISIBLE);
				}
			}
			else {
				moreButton.setVisibility(View.GONE);
			}



			loadSpinnerListener.setLoadingSpinnter(false);

		}
	}

	private class GoogleCardsAdapter extends ArrayAdapter<HashMap<String, String>> {

		private Context mContext;

		public GoogleCardsAdapter(Context context) {
			mContext = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder;
			View view = convertView;
			if (view == null) {
				view = LayoutInflater.from(mContext).inflate(R.layout.activity_googlecards_card, parent, false);

				viewHolder = new ViewHolder();
				viewHolder.textView = (TextView) view.findViewById(R.id.feed_card_text);
				viewHolder.titleView = (TextView) view.findViewById(R.id.feed_card_title);
				viewHolder.beverageTitle= (TextView) view.findViewById(R.id.feed_card_beverage_title);
				view.setTag(viewHolder);
				viewHolder.imageView = (ImageView) view.findViewById(R.id.feed_card_picture);

			} else {
				viewHolder = (ViewHolder) view.getTag();
			}
			viewHolder.textView.setText(getItem(position).get("content"));
			viewHolder.beverageTitle.setText(getItem(position).get("title"));
			viewHolder.titleView.setText(getItem(position).get("user"));
			imageLoader.displayImage(getItem(position).get("PicURL"), viewHolder.imageView);
			return view;
		}





		private class ViewHolder {
			TextView beverageTitle;
			TextView titleView;
			TextView textView;
			ImageView imageView;
		}
	}
}


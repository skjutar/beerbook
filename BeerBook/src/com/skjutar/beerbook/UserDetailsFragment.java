package com.skjutar.beerbook;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haarman.listviewanimations.itemmanipulation.ExpandableListItemAdapter;




import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


public class UserDetailsFragment extends Fragment {

	public static final String KEY_RESULT = "result";
	private static String KEY_SUCCESS = "success";
	private static String KEY_ERROR = "error";
	private static String KEY_ERROR_MSG = "error_msg";
	public static final String KEY_BEVERAGES = "beverages";
	public ListView mListView;
	public BeverageExpandableListItemAdapter mExpandableListItemAdapter;
	public ArrayList<HashMap<String, String>> result;
	private Context mContext;
	private String user;
	private Menu menu;
	private View header;
	private LoadSpinnerListener loadSpinnerListener;
	public Bitmap profilePicture;
	private FollowListener followListener;
	public boolean friends;
	private Switch followSwitch;
	private ImageView profileView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.user_details,container, false);
		header = inflater.inflate(R.layout.my_collection_header, container, false);
		profileView = (ImageView)header.findViewById(R.id.profilePicture);
		followSwitch = (Switch) header.findViewById(R.id.followSwitch);
		followSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				setFriend(isChecked);			
			}
		});
		mListView = (ListView) view.findViewById(R.id.usercollectionlist);
		mListView.setDivider(null);
		header.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT));
		mListView.addHeaderView(header);
		mExpandableListItemAdapter = new BeverageExpandableListItemAdapter(getActivity(), result);
		mExpandableListItemAdapter.setLimit(1);
		mListView.setAdapter(mExpandableListItemAdapter);


		if(savedInstanceState!=null)
		{

			result = (ArrayList<HashMap<String,String>>)savedInstanceState.getSerializable("items");
			user = savedInstanceState.getString("user");
			profilePicture = savedInstanceState.getParcelable("pic");
			friends = savedInstanceState.getBoolean("friendState");

			if(result!=null&&!result.isEmpty()){
				mExpandableListItemAdapter.addAll(result);
				mExpandableListItemAdapter.notifyDataSetChanged();
			}
			if(user!=null &&!user.isEmpty())
			{
				TextView headerText =  (TextView)header.findViewById(R.id.headerText);
				headerText.setText(user);
				followSwitch.setChecked(friends);
				followSwitch.setVisibility(View.VISIBLE);
				profileView.setImageBitmap(profilePicture);
			}
			else
			{
				TextView headerText =  (TextView)header.findViewById(R.id.headerText);
				headerText.setText("Please choose a user");
			}

		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("items", (Serializable) result);
		outState.putString("user", user);
		outState.putBoolean("friendState", friends);
		outState.putParcelable("pic", profilePicture);
	}
	
	


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			loadSpinnerListener = (LoadSpinnerListener) activity;
			followListener = (FollowListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}
	



	public void changeUser(String user)
	{
		TextView headerText =  (TextView)header.findViewById(R.id.headerText);
		if(user!=null && !user.equals(headerText.getText().toString()))
		{
			followSwitch.setVisibility(View.GONE);
			this.user=user;
			headerText.setText("");
			loadSpinnerListener.setLoadingSpinnter(true);
			new GetUserTask().execute(user);
		}
		else if(user!=null)
		{
			followSwitch.setVisibility(View.VISIBLE);
		}
		else if(headerText.getText().toString().isEmpty())
		{
			headerText.setText("Please choose a user");		
		}
	}



	private class GetUserTask extends AsyncTask<String, Void, JSONObject> {

		@Override
		protected JSONObject doInBackground(String... params) {
			BackEndService userFunction = new BackEndService();
			DatabaseHandler db = new DatabaseHandler(getActivity());
			HashMap<String, String> user = db.getUserDetails();
			String userName = user.get("name");
			JSONObject picJson = userFunction.getUserPic(params[0]);
			JSONObject isFriendJson = userFunction.checkFriend(userName, params[0]);
			String picURL = "";
			try {
				JSONObject resultJson = picJson.getJSONObject(KEY_RESULT);
				picURL = resultJson.getString("picURL");
				friends = isFriendJson.getBoolean(KEY_RESULT);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			URL img_value;
			try {
				img_value = new URL("http://graph.facebook.com/"+picURL+"/picture?type=large");
				profilePicture = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONObject beverages = userFunction.getUserBeverages(params[0]);
			return beverages;		
		}

		protected void onPostExecute(JSONObject json) {
			try {
				if (json != null && json.getString(KEY_SUCCESS) != null) 
				{
					String res = json.getString(KEY_SUCCESS);
					if(Integer.parseInt(res) == 1){
						JSONArray jArray = json.getJSONArray(KEY_BEVERAGES);
						result= new ArrayList<HashMap<String,String>>();
						for(int i=0; i<jArray.length(); i++)
						{
							JSONObject j = jArray.getJSONObject(i);
							j = j.getJSONObject("beverage");
							HashMap<String, String> item = new HashMap<String, String>();
							item.put("title", j.getString("Namn") + " "+ j.getString("Alkoholhalt"));
							item.put("id", j.getString("Artikelid"));
							item.put("content", "Date: "+
									j.getString("DATE") + "\n"+"Volume: "+
									j.getString("Volymiml")+"ml"+ "\n"+"Package: "+
									j.getString("Forpackning"));
							item.put("rating", j.getString("RATING"));
							result.add(item);
						}
						TextView headerText =  (TextView)header.findViewById(R.id.headerText);
						headerText.setText(user);
						profileView.setImageBitmap(profilePicture);

						followSwitch.setChecked(friends);
						followSwitch.setVisibility(View.VISIBLE);
						mListView.setDivider(null);
						if(mExpandableListItemAdapter==null)
						{
							mExpandableListItemAdapter = new BeverageExpandableListItemAdapter(getActivity(), result);
							mExpandableListItemAdapter.setLimit(1);
							mListView.setAdapter(mExpandableListItemAdapter);
						}
						mExpandableListItemAdapter.clear();
						mExpandableListItemAdapter.addAll(result);
						mExpandableListItemAdapter.notifyDataSetChanged();
						loadSpinnerListener.setLoadingSpinnter(false);
					}
				}
			}catch (JSONException e){
				e.printStackTrace();
			} 

		}



	}


	private class BeverageExpandableListItemAdapter extends ExpandableListItemAdapter<HashMap<String, String>> {

		private Context mContext;

		/**
		 * Creates a new ExpandableListItemAdapter with the specified list, or an empty list if
		 * items == null.
		 */
		private BeverageExpandableListItemAdapter(Context context, List<HashMap<String, String>> items) {
			super(context, R.layout.user_details_card, R.id.user_collection_card_title, R.id.user_colleciton_card_content, items);
			mContext = context;
		}



		@Override
		public View getTitleView(int position, View convertView, ViewGroup parent) {
			TextView tv = (TextView) convertView;
			if (tv == null) {
				tv = new TextView(mContext);
			}
			HashMap<String, String> item = getItem(position);
			tv.setTextSize(23);
			tv.setText(item.get("title"));
			tv.setTextColor(Color.parseColor("#222324"));
			tv.setTypeface(Typeface.DEFAULT);
			return tv;
		}

		@Override
		public View getContentView(int position, View convertView, ViewGroup parent) {
			View view = parent.findViewById(R.id.user_colleciton_card_content);
			View layout = view.findViewById(R.id.user_collection_card_content_layout);
			TextView textView = (TextView) layout.findViewById(R.id.textExpandedBeverage);

			RatingBar rBar = (RatingBar) layout.findViewById(R.id.ratingBar);

			HashMap<String, String> item = getItem(position);
			rBar.setRating(Float.parseFloat(item.get("rating")));
			textView.setText(item.get("content"));

			return layout;
		}
	}


	public void setFriend(boolean state) {
		new SetFriendTask().execute(state+"", user);


	}

	private class SetFriendTask extends AsyncTask<String, Void, JSONObject> {

		@Override
		protected JSONObject doInBackground(String... params) {
			BackEndService backEnd = new BackEndService();
			DatabaseHandler db = new DatabaseHandler(getActivity());
			HashMap<String, String> user = db.getUserDetails();
			String userName = user.get("name");
			String setFriend = params[0];
			JSONObject json;
			if(setFriend.equals("true"))
			{
				 json = backEnd.setFriend(userName, params[1]);
			}
			else {
				 json = backEnd.setUnFriend(userName, params[1]);
			}

			return json;
		}

		protected void onPostExecute(JSONObject json) {
			try {
				friends = json.getBoolean(KEY_RESULT);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

